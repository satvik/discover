/*
 *   SPDX-FileCopyrightText: 2015 Aleix Pol Gonzalez <aleixpol@blue-systems.com>
 *
 *   SPDX-License-Identifier: LGPL-2.0-or-later
 */

import QtQuick 2.4
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.1
import org.kde.discover 2.0
import org.kde.discover.app 1.0
import "navigation.js" as Navigation
import org.kde.kirigami 2.14 as Kirigami

DiscoverPage
{
    id: page
    title: i18n("Featured")
    objectName: "featured"

    actions.main: window.wideScreen ? searchAction : undefined

    readonly property bool isHome: true

    function searchFor(text) {
        if (text.length === 0)
            return;
        Navigation.openCategory(null, "")
    }

    ColumnLayout {
        anchors.centerIn: parent
        opacity: 0.5

        visible: appsRep.count === 0 && appsRep.model.isFetching

        Kirigami.Heading {
            level: 2
            Layout.alignment: Qt.AlignCenter
            text: i18n("Loading…")
        }
        BusyIndicator {
            id: indicator
            Layout.alignment: Qt.AlignCenter
            Layout.preferredWidth: Kirigami.Units.gridUnit * 4
            Layout.preferredHeight: Kirigami.Units.gridUnit * 4
        }
    }

    Loader {
        active: appsRep.count === 0 && !appsRep.model.isFetching
        anchors.centerIn: parent
        width: parent.width - (Kirigami.Units.largeSpacing * 4)
        sourceComponent: Kirigami.PlaceholderMessage {
            readonly property var helpfulError: appsRep.model.currentApplicationBackend.explainDysfunction()
            icon.name: helpfulError.iconName
            text: i18n("Unable to load applications")
            explanation: helpfulError.errorMessage

            Repeater {
                model: helpfulError.actions
                delegate: Button {
                    Layout.alignment: Qt.AlignHCenter
                    action: ConvertDiscoverAction {
                        action: modelData
                    }
                }
            }
        }
    }

    signal clearSearch()

    readonly property bool compact: page.width < 550 || !applicationWindow().wideScreen

    footer: ColumnLayout {
        spacing: 0

        Kirigami.Separator {
            Layout.fillWidth: true
            visible: Kirigami.Settings.isMobile && inlineMessage.visible
        }

        Kirigami.InlineMessage {
            id: inlineMessage
            icon.name: updateAction.icon.name
            showCloseButton: true
            Layout.fillWidth: true
            Layout.margins: Kirigami.Units.largeSpacing * 2
            text: i18n("Updates are available")
            visible: Kirigami.Settings.isMobile && ResourcesModel.updatesCount > 0
            actions: Kirigami.Action {
                icon.name: "go-next"
                text: i18nc("Short for 'show updates'", "Show")
                onTriggered: updateAction.trigger()
            }
        }
    }


    Kirigami.CardsLayout {
        id: apps
        minimumColumnWidth: Kirigami.Units.gridUnit * 20
        maximumColumns: 5

        Repeater {
            id: appsRep
            onActiveFocusChanged: if (activeFocus && currentIndex === -1) {
                currentIndex = 0;
            }
            activeFocusOnTab: true
            model: FeaturedModel {}
            delegate: ApplicationDelegate {
                application: model.application
                compact: page.compact
            }
        }
    }
}
